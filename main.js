var bool1,bool2,bool3 = false;
// var pakarToRashotBoolRight = false
// var pakarToRashotBoolLeft = false
var pakarToRashotRightCount = 0;
var pakarToRashotLeftCount = 0; 
var zrimatMidaCount = 0;
var myConfig = {
    pakarToRashot1: "ברשות המקומית טמון מידע רב אודות התכנון העירוני של מרחב הרשות, מידע פרטני אודות מבנים, קווי חשמל ביוב וכו’.<br> בנוסף, קיימים ברשות נתוני מיגון של המבנים בשטחה." ,
    pakarToRashot2: "הרשות מחזיקה מידע רב אודות פניות אזרחים לרשות במוקד המידע, אגפי החינוך המקומיים, מחלקות הרווחה והסיוע ושינויים בהרגלי צריכת החשמל והמים ",
    pakarToRashot3: "הרשות מבצעת מעקב אודות אירועי חילוץ, נפילות וכו’.<br> המידע הנאסף ברשויות מהווה תמונה משלימה לקיום האירוע.",
    pakarToRashot4: 'במצב של פיוני אוכלוסייה מהרשות המקומית, הרשות מבצע מעקב של האוכלוסייה במרחב. <br>המידע מתייחס הן למתפנים עצמאית והן לפונים המסתעיים בכוחות פקע"ר והרשו"מ.',
    pakarToRashot5: 'יחד עם העברת מדיניות ההתגוננות, היקל"ר יכול לסייע לרשות להבין את משמעויות מדיניות ההתגוננות והנחיות לניהול שגרת חיים כפי שנקבעו על ידי הפיקוד.',
    pakarToRashot6: 'תוכנית הטיפול באירוע על ידי הנפה וכוחותיה ופעילות הכוחות במרחב הרשות המקומית.<br> בנוסף מתן מענה למענה לדרישות הסיוע שהועברו מהרשות המקומית ליקל"ר.'
}
var tirgolA = {
    camera : "לחיצה על מצלמות רשותיות: נכון, מצלמות רשותיות מהוות מקור למידע ויזואלי ויכולות להוות אינדיקטור על מצב אתר ההרס במידה והייתה נפילה על מבנה. <br>יכולת מיקוד האירוע למצלמה יתאפשר רק לאחר הבנת מיקום הנפילה.",
    telephoneAlerts: 'נכון חלקית... דיווחי טלפון לרשות מהווים בסיס ליצירת תמונת מצב של אירוע הרס. <br>דיווחי טלפון ירוכזו על ידי הרשות המקומית ודרכם ניתן להתחיל ביצירת תמנ"צ לאירוע נפילה.',
    police: 'נכון, נציג משטרה עומד בקשר מול גורמי שיטור בעיר שמרכזים מידע לגבי פניות אזרחים ומיקומי שיטור בשטח.<br> שוטרים ואזרחים מאפשרים מציאת מיקום של נפילות.',
    natzig: 'נכון, מרכז ההפעלה מרכז את פעילות הרשות המקומית ואת המידע המגיע אליו מאזרחים הפונים אל הרשות.',
    kamanGdod: 'נסו שוב.. קמ"ן הגדוד יוצב בהמשך יחד עם גדוד באתר הרס על ידי מפקדת הנפה. עד להגעת הקמ"ן, הוא לא יוכל לתת מידע נוסף לגבי אתר ההרס. <br> עם זאת, בשלבי ההמשך של הטיפול, קמ"ן גדוד יוכל לתת פרטים על מצב החילוץ.',
    samlilNafa: 'נסו שוב... הנפה מרכזת את תמונת המצב ברמתה אך אין לה מידע קונקרטי על מיקום נפילה מדוייק, לא כל שכן למידע מהשטח. <br>הנפה מסתמכת על מידע המגיע אליו מהיקל"ר אותו היא מרכזת ברמתה.'
}
var flipCardCounter1 = 0,flipCardCounter2 = 0 ,flipCardCounter3 = 0,flipCardCounter4 = 0,flipCardCounter5 = 0;
$(function(){
    $("#startButton").click(startLomda);

})

 function startLomda(event) {
    $("#logoPakar").animate({
        left : "2%" ,
        top : "2%" ,
        height : "10vh" ,
        width : "10vh" 
    }),   
    $("#logoModin").animate({
        left : "9%" ,
        top : "2%" ,
        height : "10vh" ,
        width : "8vh" 
    })
    $("#titleFirstPage").removeClass("animate__bounceInLeft  animate__delay-4s");
    $("#titleFirstPage").addClass(" animate__backOutLeft");
    $("#startButton").fadeOut();
    $("#startButton").off("click",startLomda);
    $("#titleSecondPage").fadeIn(1000);
    $("#targetText").fadeIn(3000);
    $("#nextButton").fadeIn(1000);
    $("#nextButton").on("click", nextText);
 }

 function nextText(event) {
    $("#nextButton").off("click", nextText);
    $("#titleSecondPage").removeClass("animate__bounceInLeft  animate__delay-2s");
    $("#titleSecondPage").addClass(" animate__bounceOutUp")
    $("#targetText").removeClass("animate__fadeInRightBig animate__delay-2s");
    $("#targetText").addClass("animate__fadeOutRightBig")
    $("#titleThirdPage").fadeIn(1000);
    $(".list").fadeIn(1000)
    $("#soldier").fadeIn(1000);
    $("#nextButton").on("click",tfisaPikuditA);
 }

 function tfisaPikuditA(event) {
    $("#firstStep").fadeIn(1000)
    $("#titleThirdPage").fadeOut(1000);
    $("#nextButton").animate({
        bottom: "7%"
    })
    $(".list").fadeOut(1000)
    $("#soldier").fadeOut(1000);
    $("#map").fadeIn(1000)
    $("#titleFourthPage").fadeIn(1000);
    $("#tfisaPikuditText").fadeIn(1000);
    $("#nextButton").off("click",tfisaPikuditA);
    $("#nextButton").on("click",tfisaPikuditAA);
 }
function tfisaPikuditAA(event) {
    $("#tfisaPikuditText").removeClass("animate__fadeInRightBig animate__delay-2s");
    $("#tfisaPikuditText").addClass("animate__fadeOutRightBig")
    $("#tfisaPikuditTextA").fadeIn(2000);
    $("#nextButton").off("click",tfisaPikuditAA);
    $("#nextButton").on("click",tfisaPikuditAAA);
}
function tfisaPikuditAAA(event) {
    $("#nextButton").off("click",tfisaPikuditAAA);
    $("#nextButton").on("click",tfisaPikuditB);
    $("#map").fadeOut(1000)
    $("#soldier").fadeIn(1000);
    $("#tfisaPikuditTextA").fadeOut(1000);
    $("#tfisaPikuditTextAA").fadeIn(1000);
}
function tfisaPikuditB(event) {
    $("#firstStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#secondStep").fadeIn(1000)
    $("#nextButton").off("click",tfisaPikuditB);
    $("#tfisaPikuditTextAA").fadeOut(1000);
    $("#titleFourthPage").removeClass("animate__bounceInLeft animate__delay-2s");
    $("#titleFourthPage").addClass("animate__bounceOutUp")
    $("#titleFifthPage").fadeIn(1000)
    $("#MavoLyaklarA").fadeIn(1000)
    $("#nextButton").on("click",MavoLyaklarA);
}
function MavoLyaklarA(event) {
    $("#nextButton").off("click",MavoLyaklarA);
    $("#MavoLyaklarA").fadeOut(1000)
    $("#soldier").fadeOut(1000);
    $(".tRight").fadeIn(1000)
    $(".lineR").fadeIn(1000)
    $("#yaklar").fadeIn(1000)
    $("#yaklarText1").fadeIn(1000)
    setTimeout(wobleYaklar, 6000);
    $("#yaklar").on("click",leftTree)
    $("#nextButton").prop('disabled', true);
}
function wobleYaklar(){
    $("#yaklar").removeClass("animate__backInRight animate__delay-2s");
    $("#yaklar").addClass("animate__wobble");
}
function leftTree(event) {
    $("#yaklar").off("click",leftTree)
    $("#yaklar").css({
        backgroundColor: "#5699ce"
    })
    $("#yaklarText1").fadeOut(1000)
    $(".tLeft").fadeIn(1000)
    $(".lineL").fadeIn(1000)
    $("#yaklarText2").fadeIn(1000)
    $("#nextButton").prop('disabled', false);
    $("#nextButton").on("click",MavoLyaklarB)
}
function MavoLyaklarB(event) {
    $("#nextButton").off("click",MavoLyaklarB)
    $(".tRight").removeClass("animate__backInRight animate__delay-2s");
    $(".tRight").addClass("animate__bounceOutDown");
    $(".tLeft").removeClass("animate__backInRight animate__delay-2s");
    $(".tLeft").addClass("animate__bounceOutDown");
    $(".lineR").removeClass("animate__backInRight animate__delay-2s");
    $(".lineR").addClass("animate__bounceOutDown");
    $(".lineL").removeClass("animate__backInRight animate__delay-2s");
    $(".lineL").addClass("animate__bounceOutDown")
    $("#yaklar").addClass("animate__bounceOutDown");
    $("#yaklarText2").fadeOut(1000)
    $("#mavoLyaklarText").fadeIn(1000)
    $("#soldier").fadeIn(1000);
    $("#nextButton").on("click",MavoLyaklarC)
}
function MavoLyaklarC(event) {
    $("#secondStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#thirdStep").fadeIn(1000)
    $("#mavoLyaklarText").fadeOut(1000);
    $("#soldier").fadeOut(1000);
    $("#nextButton").off("click",MavoLyaklarC)
    $("#nextButton").on("click",MavoLyaklarC1)
    $("#divMavoLyaklarText1").fadeIn(1000);
}
function MavoLyaklarC1(event) {
    $("#nextButton").off("click",MavoLyaklarC1)
    $("#nextButton").fadeOut(1000);
    $("#divMavoLyaklarText1").fadeOut(1000);
    $("#mavoLyaklarText2").fadeIn(1000);
    $("#dragDrop").fadeIn(1000)
    
    $("#drop1").droppable({
        accept: "#drag1",
        drop: function(event,ui) {
            bool1 = true
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-3%",
                left: "1%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "2.6vh"
            })
        }
    })
    $("#drop2").droppable({
        accept: "#drag2",
        drop: function(event,ui) {
            bool2 = true
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-3%",
                left: "1%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize: "2.6vh"
            })
        }
    })
    $("#drop3").droppable({
        accept: "#drag3",
        drop: function(event,ui) {
            bool3 = true
          $(ui.draggable).appendTo(this)
          $(ui.draggable).draggable("disable")
          $(ui.draggable).css({
              top: "-3%",
              left: "1%",
              backgroundColor: "green",
              borderColor: "green",
              textAlign: "center",
              fontSize: "2.6vh"
          })
        }
    })
    $("#drag1").draggable({
        revert: "invalid"
    })
    $("#drag2").draggable({
        revert: "invalid"
    })
    $("#drag3").draggable({
        revert: "invalid"
    })
    myInterval = setInterval(function(){  if ((bool1 && bool2 && bool3) == true) {
        $("#nextButton").fadeIn();
        clearInterval(myInterval);
     }},2000)
     $("#nextButton").on("click",MavoLyaklarD)
}
function MavoLyaklarD(event) {
    $("#nextButton").off("click",MavoLyaklarD)
    $("#dragDrop").addClass("animate__animated animate__bounceOutDown");
    $("#mavoLyaklarText2").removeClass("animate__delay-4s animate__bounceOutRight")
    $("#mavoLyaklarText2").addClass("animate__bounceOutRight")
    $("#mavoLyaklarText3").fadeIn(1000)
    for (var i = 1; i<6; i++) {
        $("#flipCard" + i).fadeIn(1000)
    }
    $("#verticalLine1").fadeIn(1000)
    $("#horizonLine1").fadeIn(1000)
    $("#horizonLine2").fadeIn(1000)
    $("#nextButton").prop('disabled', true);
    $("#flipCard1").mouseover(function(){flipCardCounter1++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )&& (flipCardCounter3 > 0 )&& (flipCardCounter4 > 0 )&& (flipCardCounter5 > 0 )) {
            $("#nextButton").prop('disabled', false);
        }})
    $("#flipCard2").mouseover(function(){flipCardCounter2++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )&& (flipCardCounter3 > 0 )&& (flipCardCounter4 > 0 )&& (flipCardCounter5 > 0 )) {
            $("#nextButton").prop('disabled', false);
        }})
    $("#flipCard3").mouseover(function(){flipCardCounter3++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )&& (flipCardCounter3 > 0 )&& (flipCardCounter4 > 0 )&& (flipCardCounter5 > 0 )) {
            $("#nextButton").prop('disabled', false);
        }})
    $("#flipCard4").mouseover(function(){flipCardCounter4++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )&& (flipCardCounter3 > 0 )&& (flipCardCounter4 > 0 )&& (flipCardCounter5 > 0 )) {
            $("#nextButton").prop('disabled', false);
        }})
    $("#flipCard5").mouseover(function(){flipCardCounter5++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )&& (flipCardCounter3 > 0 )&& (flipCardCounter4 > 0 )&& (flipCardCounter5 > 0 )) {
            $("#nextButton").prop('disabled', false);
        }})
    $("#nextButton").on("click",MavoLyaklarE)
}
function MavoLyaklarE(event) {
    $("#nextButton").off("click",MavoLyaklarE)
    $("#mavoLyaklarText3").fadeOut(1000);
    $(".line").fadeOut();
    for (var i = 1; i<6; i++) {
        $("#flipCard" + i).removeClass("animate__bounceInUp animate__delay-3s")
        $("#flipCard" + i).addClass("animate__bounceOutDown")
    }
    $("#titleFifthPage").css({
        "top" : "0%"
    })
    $("#nextButton").prop('disabled', true);
    $("#firstTree").fadeIn(1000)
    $("#village").on("click", function(){
        $("#village").off("click",function(){})
        $("#firstTree").fadeOut(1000)
        $("#secondTree").fadeIn(1000)
    })
    $("#town2").on("click", function(){
        $("#town2").off("click",function(){})
        $("#secondTree").fadeOut(1000)
        $("#thirdTree").fadeIn(1000)
    })
    $("#city3").on("click", function(){
        $("#city3").off("click",function(){})
        $("#thirdTree").fadeOut(1000)
        $("#nextButton").prop('disabled', false);
    })
    $("#mavoLyaklarText4").fadeIn(1000);
    $("#nextButton").on("click",MavoLyaklarF)
}
function MavoLyaklarF(event){
    $("#nextButton").off("click",MavoLyaklarF)
    $("#titleFifthPage").fadeOut(1000);
    $("#mavoLyaklarText4").fadeOut(1000);
    $("#mavoLyaklarText5").fadeIn(1000);
    $("#soldier").fadeIn(1000);
    $("#titleSixPage").fadeIn(1000);
    $("#nextButton").on("click",zrimatMida)

}
function zrimatMida(event){
    $("#nextButton").prop('disabled', true);
    $("#nextButton").off("click",zrimatMida)
    $("#mavoLyaklarText5").fadeOut(1000);
    $("#nextButton").on("click",zrimatMidaA)
    $("#soldier").fadeOut(1000);
    $("#textZrima").fadeIn(1000);
    $("#textZrima1").fadeIn(1000);
    $("#zrimatMidaRight").fadeIn(1000);
    $("#zrimatMidaLeft").fadeIn(1000);
    $("#zrimatMidaRight").on("click",function(){
        pakarToRashotRightCount++;
        if (pakarToRashotRightCount % 2 !== 0) {
            $("#pakarToRashot").fadeIn(1000)
            $("#pakarToRashot").css({
                display: "flex"
            })

            $(".topicPakarToRashot").on("click",pakarToRashotText)
        }else {
            $("#pakarToRashot").fadeOut(1000)
        }
    })
    $("#zrimatMidaLeft").on("click",function(){
        pakarToRashotLeftCount++;
        if (pakarToRashotLeftCount % 2 !== 0) {
            $("#pakarToRashotDiv").fadeIn(1000)
            $("#pakarToRashotDiv").css({
                display: "flex"
            })

            $(".topicPakarToRashot").on("click",pakarToRashotText)
        }else {
            $("#pakarToRashotDiv").fadeOut(1000)
        }
    })
    var setInter = setInterval(function(){
        if ((pakarToRashotLeftCount > 0) &&(pakarToRashotRightCount > 0) ){
            $("#nextButton").prop('disabled', false);
            clearInterval(setInter)
        }
    },2000)
    

}
function pakarToRashotText(event) {
    $("#pakarToRashotBox").fadeIn(1000)
            var tId = this.id
            $("#pakarToRashotText").html(myConfig[tId]) 
            $("#button1").on("click",function(){
                $("#pakarToRashotBox").fadeOut(1000)
            })
}
function zrimatMidaA(event) {
    $("#nextButton").off("click",zrimatMidaA)
    $("#textZrima").fadeOut(1000);
    $("#textZrima1").fadeOut(1000);
    $("#zrimatMidaRight").fadeOut(1000);
    $("#zrimatMidaLeft").fadeOut(1000);
    $("#pakarToRashotDiv").fadeOut(1000)
    $("#pakarToRashot").fadeOut(1000)
    $("#map").fadeIn(1000)
    $("#zrimatMidaText1").fadeIn(1000)
    $("#nextButton").on("click",zrimatMidaB)
}
function zrimatMidaB(event) {
    $("#nextButton").off("click",zrimatMidaB)
    $("#map").fadeOut(1000)
    $("#map1").fadeIn(1000)
    $("#zrimatMidaText1").fadeOut(1000)
    $("#zrimatMidaText2").fadeIn(1000)
    $("#nextButton").on("click",zrimatMidaC)
}
function zrimatMidaC(event) {
    $("#nextButton").prop('disabled', true);
    $("#nextButton").off("click",zrimatMidaC)
    $("#map1").fadeOut(1000)
    $("#zrimatMidaText2").fadeOut(1000)
    $("#dragDrop2").fadeIn(1000)
    $("#button").on("click",closeBox)
    $("#seconedDrop1").droppable({
        accept: ".nafa",
        drop: function(event,ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize:"2.5vh"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafa" id="seconedDrag4">נפה</div>')
            $("#seconedDrag4").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button").fadeIn(1000)
            $("#textBox").html("המידע אודות נפילות ואירועי נזק מהווה אינדיקציה לנפה אודות האירועים במרחב. על ידי העברת המידע המדויק ניתן יהיה להחליט על ויסותי כוחות ושליחת צוותים לאירועים.")
            zrimatMidaCount++;
            if (zrimatMidaCount == 5){
                $("#nextButton").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop2").droppable({
        accept: ".nafaGdod",
        drop: function(event,ui) {
            $(ui.draggable).appendTo(this)
            $(ui.draggable).draggable("disable")
            $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
                backgroundColor: "green",
                borderColor: "green",
                textAlign: "center",
                fontSize:"2.5vh"
            })
            $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag5">נפה וגדוד</div>')
            $("#seconedDrag5").draggable({
                revert: "invalid"
            })
            $("#boxText").fadeIn(1000)
            $("#button").fadeIn(1000)
            $("#textBox").html('אירועים בטחוניים במרחב משפיעים על יכולת הפעולה של כוחות פקע"ר. נתונים אלה הכרחיים לנפה לשם יצירת תמנ"צ וכן לגדוד במידה ואירועים קורים בקרבתו.')
            zrimatMidaCount++;
            if (zrimatMidaCount == 5){
                $("#nextButton").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop3").droppable({
        accept: ".nafa",
        drop: function(event,ui) {
          $(ui.draggable).appendTo(this)
          $(ui.draggable).draggable("disable")
          $(ui.draggable).css({
            top: "-7.5%",
            left: "1.5%",
              backgroundColor: "green",
              borderColor: "green",
              textAlign: "center",
              fontSize:"2.5vh"
          })
          $("#dragDiv1").append('<div class="seconedDropable nafa" id="seconedDrag4">נפה</div>')
          $("#seconedDrag4").draggable({
            revert: "invalid"
        })
        $("#boxText").fadeIn(1000)
        $("#button").fadeIn(1000)
        $("#textBox").html('פגיעה הרציפות התפקודית   לשם קבלת החלטות בנוגע להגשת סיוע לרשויות מתקשות, הנפה צריכה תמונה אחודה של מצבי הרשויות בשטחה. הגדוד לעומת זאת, עוסק בתא שטח מוגדר ואינו זקוק למידע אודות התמודדות הרשות.')
        zrimatMidaCount++;
            if (zrimatMidaCount == 5){
                $("#nextButton").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop4").droppable({
        accept: ".nafaGdod",
        drop: function(event,ui) {
          $(ui.draggable).appendTo(this)
          $(ui.draggable).draggable("disable")
          $(ui.draggable).css({
            top: "-7.5%",
            left: "1.5%",
              backgroundColor: "green",
              borderColor: "green",
              textAlign: "center",
              fontSize:"2.5vh"
          })
          $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag5">נפה וגדוד</div>')
          $("#seconedDrag5").draggable({
            revert: "invalid"
        })
        $("#boxText").fadeIn(1000)
        $("#button").fadeIn(1000)
        $("#textBox").html('עומסים בצירים ראשיים   המידע אודות תנועה הצירים הינו בעל השלכות לתנועת אזרחים וכוחות בשטח. המידע רלוונטי הן לנפה והן לגדוד במידה ויש לו צורך בהעברת ציוד וכוחות אל ומאתר הרס')
        zrimatMidaCount++;
            if (zrimatMidaCount == 5){
                $("#nextButton").prop('disabled', false);
            }
        }
    })
    $("#seconedDrop5").droppable({
        accept: "#seconedDrag3",
        drop: function(event,ui) {
          $(ui.draggable).appendTo(this)
          $(ui.draggable).draggable("disable")
          $(ui.draggable).css({
                top: "-7.5%",
                left: "1.5%",
              backgroundColor: "green",
              borderColor: "green",
              textAlign: "center",
              fontSize:"2.5vh"
          })
          $("#dragDiv1").append('<div class="seconedDropable nafaGdod" id="seconedDrag6">גדוד</div>')
          $("#seconedDrag6").draggable({
            revert: "invalid"
        })
        $("#boxText").fadeIn(1000)
        $("#button").fadeIn(1000)
        $("#textBox").html('מידע הנדסי הכרחי לגדוד לשם ביצוע משימתו שכן כך ניתן להבין את אופן קריסת המבנה, מיקומים של נעדרים ומהווה בסיס לחלוקת האתר לשם עבודות חילוץ.')
        zrimatMidaCount++;
            if (zrimatMidaCount == 5){
                $("#nextButton").prop('disabled', false);
            }
        }
    })
    $("#seconedDrag1").draggable({
        revert: "invalid",
       
    })
    $("#seconedDrag2").draggable({
        revert: "invalid",

    })
    $("#seconedDrag3").draggable({
        revert: "invalid"
    })
    function closeBox(event){
        $("#boxText").fadeOut(1000)
    }
    $("#nextButton").on("click",MarchotMidaA)
}
function MarchotMidaA(event){
    $("#nextButton").off("click",MarchotMidaA)
    $("#dragDrop2").fadeOut(1000)
    $("#titleSixPage").fadeOut(1000)
    $("#thirdStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#fourthStep").fadeIn(1000)
    $("#titleSevenPage").fadeIn(1000)
    $("#marchotMidaText").fadeIn(1000)
    $("#book").fadeIn(1000)
    $("#textBook").fadeIn(1000)
    $("#driveLink").fadeIn(1000)
    $("#marchotMidaVideo").fadeIn(1000)
    $("#marchotMidaVideo").css({
        display: "flex"
    })
    $("#nextButton").on("click",parakE)
}
function parakE(event){
    $("#nextButton").off("click",parakE)
    $("#titleSevenPage").fadeOut(1000)
    $("#marchotMidaText").fadeOut(1000)
    $("#book").fadeOut(1000)
    $("#textBook").fadeOut(1000)
    $("#driveLink").fadeOut(1000)
    $("#marchotMidaVideo").fadeOut(1000)
    $("#fourthStep").css({
        backgroundColor: "green",
        borderColor: "green",
        opacity: "50%"
    })
    $("#fifthStep").fadeIn(1000)
    $("#titleEightPage").fadeIn(1000)
    $("#parakEText").fadeIn(1000)
    $("#nextButton").on("click",tirgol1)

}
function tirgol1(event) {
    $(".graphic").fadeIn(1000)
    $("#nextButton").off("click",tirgol1)
    $("#titleEightPage").fadeOut(1000)
    $("#parakEText").removeClass("animate__fadeInRightBig animate__delay-2s");
    $("#parakEText").addClass("animate__backOutRight");
    $("#titleTirgolPage").fadeIn(1000)
    setTimeout(function(){
        $(".tirgolDiv").fadeIn(1000)
    },1000)
    $("#tirgolText1").fadeIn(1000)
    $(".tirgolDiv").on("click",tirgolAText)
    $("#nextButton").on("click",tirgolB1)
}
function tirgolAText(event) {
    $("#pakarToRashotBox").fadeIn(1000)
            var tId = this.id
            if ((tId == "samlilNafa") || (tId == "kamanGdod")) {
                $("#pakarToRashotBox").css({
                    backgroundColor: "red",
                    borderColor: "red"
                })
                $("#" + tId).css({
                    backgroundColor: "red",
                    borderColor: "red"
                })
            }else {
                $("#pakarToRashotBox").css({
                backgroundColor: "green",
                borderColor: "green"
            })
            $("#" + tId).css({
                backgroundColor: "green",
                borderColor: "green"
            })
            }
            $("#pakarToRashotText").html(tirgolA[tId]) 
            $("#button1").on("click",function(){
                $("#pakarToRashotBox").fadeOut(1000)
            })
}
function tirgolB1(event){
    $("#nextButton").off("click",tirgolB1)
    $("#tirgol1").fadeOut(1000)
    $("#tirgolBoxDiv").fadeOut(1000)
    $("#tirgolB1Text").fadeIn(1000)
    $("#nextButton").on("click",tirgolB)
}
function tirgolB(event) {
    $("#kamanPicture").css({
        "top" : "40%"
    })
    $("#policePicture").css({
        "top" : "40%"
    })
    $("#natzigPicture").css({
        "top" : "40%"
    })
    $("#nextButton").off("click",tirgolB)
    $("#nextButton").on("click",endLomda)
    $(".tirgolDiv").off("click",tirgolAText)
    $(".tirgolDiv").css({
        backgroundColor: "#5699ce",
        borderColor: "#5699ce"})
    $("#titleTirgolBPage").fadeIn(1000)
    $("#answerFlex").fadeIn(1000)
    $("#answerFlex").css({
        display: "flex"
    })
    $("#tirgolB1Text").slideUp(1000)
    $("#tirgolBoxDiv").fadeIn(1000)
    $("#tirgolText2").fadeIn(1000)
    $("#boxText").css({
        backgroundColor: "green"
    })
    $(".answerDiv").draggable({
        revert: "invalid"
    })
    $("#camera").droppable({
        accept: "#hasimatZir",
        drop: function(event,ui) {
            $(ui.draggable).remove()
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function(){
                $("#tirgolBoxText").fadeOut(1000)
            },3000)
        }
    })
    $("#police").droppable({
        accept: "#hasimatZir",
        drop: function(event,ui) {
            $(ui.draggable).remove()
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function(){
                $("#tirgolBoxText").fadeOut(1000)
            },3000)
        }
        
    })
    $("#natzig").droppable({
        accept: "#hasimatZir",
        accept: "#electricBreak",
        drop: function(event,ui) {
            $(ui.draggable).remove(),  
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function(){
                $("#tirgolBoxText").fadeOut(1000)
            },3000)
        }
    })
    $("#kamanGdod").droppable({
        accept: "#kamotLecodim",
        drop: function(event,ui) {
            $(ui.draggable).remove(),  
            $("#tirgolBoxText").fadeIn(1000)
            setTimeout(function(){
                $("#tirgolBoxText").fadeOut(1000)
            },3000)
        }
    })
}
function endLomda(event) {
    $("#titleTirgolBPage").fadeOut(1000)
    $("#answerFlex").fadeOut(1000)
    $(".tirgolDiv").fadeOut(1000)
    $("#tirgolB1Text").fadeIn(1000)
    $("#tirgolText2").fadeOut(1000)
    $("#tirgolText2").removeClass("animate__fadeInRightBig animate__delay-2s");
    $("#tirgolText2").addClass("animate__fadeOutDown");
    $("#tirgolBoxDiv").fadeOut(1000)
    $("#nextButton").off("click",endLomda)
    $("#nextButton").fadeOut(1000)
    $("#finishButton").fadeIn(1000)
    $("#finishButton").on("click",function(){
        $("#tirgolB1Text").removeClass("animate__fadeInRightBig animate__delay-2s");
        $("#tirgolB1Text").addClass("animate__backOutRight");
        $("#endLomdaText").fadeIn(1000)
        $("#soldier").fadeIn(1000);
        $("#finishButton").fadeOut(1000)
    })
}
